export interface Pedido{
  pedido: string;
  id: string;
  cliente: string;
  endereco: string;
  status: string;
  horario: string;
  pizzas: string;
  pratos: string;
  bebidas: string;
  sobremesas: string;
}
